# -*- coding:utf-8 -*-

#2-1
# print 100 ~ 999 ABC = A*A*A + B*B*B + C*C*C
for i in range(100, 999) :
  if pow(i / 100 , 3) + pow(i % 100 / 10, 3) + pow(i % 10, 3) == i:
    print i;
    
def f(x) : 
  if pow(x / 100, 3) + pow(x % 100 /10, 3) + pow(x % 10, 3) == x:
   print x
filter(f, range(100, 999))

#2-2
# input 2 1 6
# input 2 1 3
# output Case 1: 41
# output Case 2: No answer
def f2(a, b, c):
  for i in range(10, 100) :
    if i % 3 == a and i % 5 == b and i % 7 == c :
       print i;
    else :
      continue
    print "No answer"

f2(2, 1, 6) # 2 1 6
f2(2, 1, 3) # No answer

#2-3a trangle
# #########
#  #######
#   #####
#    ###
#     #

def f3(n):
  for i in reversed(range(1, n + 1)):
    print ' '*(n-i) + '#'*(2*i-1) + ' '*(n-i) 
    
f3(5)

#2-4
#input 2 4
#65536 655360
#0 0
#Output Case 1: 0.42361
#Output Case 2: 0.00001

def f4(n, m):
  if n == 0 and m == 0 :
    return
  result = 0
  for i in range(n, m + 1):
    result += 1.0/pow(i, 2)
  print '%.5f' % result

f4(2, 4)
f4(65535, 655360)
f4(0, 0)

#2-5
# print a/b at c scala
def f5(a, b ,c):
  if a == b == c == 0:
    return
  print '%.{0}f'.format(c) %(float(a)/b)

f5(1, 6, 4)
f5(0, 0, 0)

