# -*- coding:utf-8 -*-
# This is python print format basic reference. Used for output in your way.
# All of the code written by python 2.7.x
import sys

a = 5
print a # 5

b = 0.5
print b # 0.5

d = a * b
print d # 2.5
print '%f' %d # 2.500000
print '%r' %d # 2.5
print '%.3f' %d # 2.500

e = 5 / 4
print e # 1
print '%d' %e # 1
print '%f' %e # 1.000000
print '%2.9f' %e #1.000000000

name = 'Jack'
age = 26
print '%s is %d years old' %(name, age) # Jack is 26 years old
print '%r is %r years old' %(name, age) # 'Jack' is 26 years old
print '%r is %f years old' %(name, age) # 'Jack' is 26.000000 years old

arr = [1, 2, 3 ,4]
print arr # [1, 2, 3, 4]
print tuple(arr) # (1, 2, 3, 4)
print '%r' %arr #[1, 2, 3, 4]

arr2 = ['a', 'b', 'c', 'd']
print arr2 # ['a', 'b', 'c', 'd']
print ' '.join(arr2) # a b c d
print '-'.join(arr2) # a-b-c-d

tpl = (1, 2, 3)
print "This is a tuple %r " %(tpl,) # This is a tuple (1, 2, 3)

dic = {1:'1', 2:'2'}
print dic # {1: '1', 2: '2'}

s = 'A string'
print str(s) # A string
print repr(s) # 'A string'

#str.format()
print '{0} and {1}'.format('You', 'me') # You and me
print '{1} and {0}'.format('You', 'me') # me and You
print '{0} and {1} and {whom}'.format('You', 'me', whom='she') # You and me and she
print 'PI is {0:.3f}'.format(3.1415926) # PI is 3.142
print 'String {0:10} = {1:10}'.format('name', 'Jack') # String name      = Jack
